import { ListState } from './list/types'
import { DetailsState } from './details/types'

export type RootState = {
  list: ListState
  details: DetailsState
}

export enum FETCHING_STATUS {
  IDLE = 'FETCHING_STATUS.IDLE',
  LOADING = 'FETCHING_STATUS.LOADING',
  SUCCESS = 'FETCHING_STATUS.SUCCESS',
  ERROR = 'FETCHING_STATUS.ERROR',
}

export type FetchingStatusStrings = keyof typeof FETCHING_STATUS

export type ActionType = {
  type: string
  payload?: any
}
