import { combineReducers } from 'redux'
import { reducer as listReducer } from './list/reducer'
import { reducer as detailsReducer } from './details/reducer'

export const rootReducer = combineReducers({
  list: listReducer,
  details: detailsReducer,
})
