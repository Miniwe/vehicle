import { VehiclesListItemParams } from '../../Vehicles/types'
import { FetchingStatusStrings } from '../types'

export const LIST_LOAD_NEXT = '@LIST/LOAD_NEXT'
export const LIST_FETCH_INIT = '@LIST/FETCH_INIT'
export const LIST_FETCH_SUCCESS = '@LIST/FETCH_SUCCESS'
export const LIST_FETCH_ERROR = '@LIST/FETCH_ERROR'

export type ListState = {
  items: VehiclesListItemParams[]
  fetchedItems: number
  hasMore: boolean
  status: FetchingStatusStrings
  error: string | undefined
  page: number
}

export type ActionType = {
  type: string
  payload?: any
}
