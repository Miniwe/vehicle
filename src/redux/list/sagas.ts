import axios from 'axios'
import { select, put, call, takeEvery } from 'redux-saga/effects'
import { fetchInit, fetchSuccess, fetchError } from './actions'
import { LIST_LOAD_NEXT } from './types'

function* fetchNextItems() {
  const page = yield select(({ list }) => list.page)

  try {
    yield put(fetchInit())
    const result = yield call(
      async () =>
        await axios(
          `https://vpic.nhtsa.dot.gov/api/vehicles/getallmanufacturers?format=json&page=${page}`,
        ),
    )
    if (result.data && 'Results' in result.data) {
      yield put(fetchSuccess(result.data.Results))
    } else {
      throw new Error('No results')
    }
  } catch (error) {
    yield put(fetchError(error.message))
  }
}

export function* listWatcher() {
  yield takeEvery(LIST_LOAD_NEXT, fetchNextItems)
}
