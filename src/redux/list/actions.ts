import { VehiclesListItemParams } from '../../Vehicles'
import {
  LIST_LOAD_NEXT,
  LIST_FETCH_INIT,
  LIST_FETCH_SUCCESS,
  LIST_FETCH_ERROR,
} from './types'

export function loadMoreItems() {
  return {
    type: LIST_LOAD_NEXT,
  }
}

export function fetchInit() {
  return {
    type: LIST_FETCH_INIT,
  }
}

export function fetchSuccess(items: VehiclesListItemParams[]) {
  return {
    type: LIST_FETCH_SUCCESS,
    payload: { items },
  }
}

export function fetchError(error: string) {
  return {
    type: LIST_FETCH_ERROR,
    payload: { error },
  }
}
