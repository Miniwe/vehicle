import {
  ActionType,
  LIST_LOAD_NEXT,
  LIST_FETCH_INIT,
  LIST_FETCH_SUCCESS,
  LIST_FETCH_ERROR,
} from './types'
import { FETCHING_STATUS } from '../types'

const initialState = {
  items: [],
  fetchedItems: 0,
  hasMore: true,
  status: FETCHING_STATUS.IDLE,
  page: 0,
}

export const reducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case LIST_LOAD_NEXT:
      return { ...state, page: state.page + 1 }
    case LIST_FETCH_INIT:
      return { ...state, status: FETCHING_STATUS.LOADING, error: undefined }
    case LIST_FETCH_SUCCESS:
      const moreItems = [...state.items, ...action.payload.items]
      return {
        ...state,
        status: FETCHING_STATUS.SUCCESS,
        items: moreItems,
        fetchedItems: moreItems.length,
        hasMore: state.fetchedItems < moreItems.length,
      }
    case LIST_FETCH_ERROR:
      return {
        ...state,
        status: FETCHING_STATUS.ERROR,
        error: action.payload.error,
      }
    default:
      return { ...state }
  }
}
