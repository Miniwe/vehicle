import axios from 'axios'
import { select, all, put, call, takeEvery } from 'redux-saga/effects'
import {
  fetchInit,
  fetchSuccess,
  fetchError,
  fetchMakesSuccess,
  fetchModelsSuccess,
} from './actions'
import { DETAILS_LOAD_INIT, MakeProps } from './types'

function clearName(name = '') {
  const nameArr = name.split(',')
  if (nameArr.length) {
    return nameArr[0].replace(/\s+/g, '_').toLowerCase()
  } else {
    return name.replace(/\s+/g, '_').toLowerCase()
  }
}

async function fetchDetails(detailsId: string) {
  const result = await axios(
    `https://vpic.nhtsa.dot.gov/api/vehicles/GetManufacturerDetails/${detailsId}?format=json`,
  )
  if (!result || !result.data || !('Results' in result.data)) {
    throw new Error('No Details')
  }
  return result.data.Results.shift()
}

async function fetchMakes(mfrName: string) {
  const clearedName = clearName(mfrName)
  const result = await axios(
    `https://vpic.nhtsa.dot.gov/api/vehicles/GetMakeForManufacturer/${clearedName}?format=json`,
  )
  if (!result || !result.data || !('Results' in result.data)) {
    throw new Error('No Makes')
  }
  return result.data.Results
}

async function fetchModels(makeId: string) {
  const result = await axios(
    `https://vpic.nhtsa.dot.gov/api/vehicles/GetModelsForMakeId/${makeId}?format=json`,
  )

  if (!result || !result.data || !('Results' in result.data)) {
    throw new Error(`No Makes: ${makeId}`)
  }

  return result.data.Results
}

function* fetchAllModels(makes: MakeProps[]) {
  const requests = makes.map((make) =>
    call(fetchModels, make.Make_ID as string),
  )
  return yield all(requests)
}

function* loadDetails() {
  const detailsId = yield select(({ details }) => details.detailsId)

  try {
    yield put(fetchInit())
    const details = yield call(() => fetchDetails(detailsId))
    yield put(fetchSuccess(details))

    const makes = yield call(() => fetchMakes(details.Mfr_Name))
    yield put(fetchMakesSuccess(makes))

    const models = yield call(() => fetchAllModels(makes))
    yield put(fetchModelsSuccess(models))

    console.log('EEEHUUU')
  } catch (error) {
    yield put(fetchError(error.message))
  }
}

export function* detailsWatcher() {
  yield takeEvery(DETAILS_LOAD_INIT, loadDetails)
}
