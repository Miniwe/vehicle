// import { VehicleParams } from '../../Vehicles/types'
import { FetchingStatusStrings } from '../types'

export const DETAILS_LOAD_INIT = '@DETAILS/LOAD_INIT'
export const DETAILS_FETCH_INIT = '@DETAILS/FETCH_INIT'
export const DETAILS_FETCH_SUCCESS = '@DETAILS/FETCH_SUCCESS'
export const DETAILS_FETCH_ERROR = '@DETAILS/FETCH_ERROR'
export const DETAILS_FETCH_MAKES = '@DETAILS/FETCH_MAKES'
export const DETAILS_FETCH_MODELS = '@DETAILS/FETCH_MODELS'

export type DetailsState = {
  detailsId: string | undefined
  details: any
  makes: []
  models: []
  status: FetchingStatusStrings
  error: string | undefined
}

export type ActionType = {
  type: string
  payload?: any
}

export type MakeProps = {
  Make_ID: number | string
  Make_Name: string
  Mfr_Name: string
}
