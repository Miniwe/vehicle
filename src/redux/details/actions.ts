import { VehicleParams } from '../../Vehicles'
import {
  DETAILS_LOAD_INIT,
  DETAILS_FETCH_INIT,
  DETAILS_FETCH_SUCCESS,
  DETAILS_FETCH_ERROR,
  DETAILS_FETCH_MAKES,
  DETAILS_FETCH_MODELS,
} from './types'

export function loadDetails(detailsId: string) {
  return {
    type: DETAILS_LOAD_INIT,
    payload: { detailsId },
  }
}

export function fetchInit() {
  return {
    type: DETAILS_FETCH_INIT,
  }
}

export function fetchSuccess(details: VehicleParams[]) {
  return {
    type: DETAILS_FETCH_SUCCESS,
    payload: { details },
  }
}

export function fetchMakesSuccess(makes: []) {
  return {
    type: DETAILS_FETCH_MAKES,
    payload: { makes },
  }
}
export function fetchModelsSuccess(models: []) {
  return {
    type: DETAILS_FETCH_MODELS,
    payload: { models },
  }
}

export function fetchError(error: string) {
  return {
    type: DETAILS_FETCH_ERROR,
    payload: { error },
  }
}
