import {
  ActionType,
  DETAILS_LOAD_INIT,
  DETAILS_FETCH_INIT,
  DETAILS_FETCH_SUCCESS,
  DETAILS_FETCH_ERROR,
  DETAILS_FETCH_MAKES,
  DETAILS_FETCH_MODELS,
} from './types'
import { FETCHING_STATUS } from '../types'

const initialState = {
  detailsId: undefined,
  details: {},
  makes: [],
  models: [],
  status: FETCHING_STATUS.IDLE,
}

export const reducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case DETAILS_LOAD_INIT:
      return { ...state, detailsId: action.payload.detailsId }
    case DETAILS_FETCH_INIT:
      return { ...state, status: FETCHING_STATUS.LOADING, error: undefined }
    case DETAILS_FETCH_SUCCESS:
      return {
        ...state,
        status: FETCHING_STATUS.SUCCESS,
        details: action.payload.details,
      }
    case DETAILS_FETCH_MAKES:
      return {
        ...state,
        makes: action.payload.makes,
      }
    case DETAILS_FETCH_MODELS:
      return {
        ...state,
        models: action.payload.models,
      }
    case DETAILS_FETCH_ERROR:
      return {
        ...state,
        status: FETCHING_STATUS.ERROR,
        error: action.payload.error,
      }
    default:
      return { ...state }
  }
}
