import { all, fork } from 'redux-saga/effects'
import { listWatcher } from './list/sagas'
import { detailsWatcher } from './details/sagas'

export function* rootSagas() {
  yield all([fork(listWatcher), fork(detailsWatcher)])
}
