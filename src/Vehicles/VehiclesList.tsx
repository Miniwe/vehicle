import React, { useEffect } from 'react'
import { Alert, Table } from 'react-bootstrap'
import InfiniteScroll from 'react-infinite-scroll-component'
import { useDispatch, shallowEqual, useSelector } from 'react-redux'
import { Loading } from '../components'
import { RootState } from '../redux/types'
import { ListActions } from '../redux/actions'
import VehicleListItem from './VehiclesListItem'
import { VehiclesListItemParams } from './index'

function VehiclesList() {
  const dispatch = useDispatch()
  const listData = useSelector((state: RootState) => state.list, shallowEqual)
  const { items, hasMore, error } = listData

  useEffect(() => {
    dispatch(ListActions.loadMoreItems())
  }, [])

  if (error) {
    return <Alert variant="danger">{error}</Alert>
  }

  return (
    <InfiniteScroll
      dataLength={items.length}
      next={() => dispatch(ListActions.loadMoreItems())}
      hasMore={hasMore}
      loader={<Loading />}
    >
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Country</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {items.map((item: VehiclesListItemParams, index: number) => (
            <VehicleListItem key={`${item.Mfr_ID}_${index}`} {...item} />
          ))}
        </tbody>
      </Table>
    </InfiniteScroll>
  )
}

export default VehiclesList
