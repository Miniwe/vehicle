import React, { useEffect } from 'react'
import { Alert } from 'react-bootstrap'
import { useDispatch, shallowEqual, useSelector } from 'react-redux'
import { FETCHING_STATUS, RootState } from '../redux/types'
import { DetailsActions } from '../redux/actions'
import { Loading } from '../components'
import { VehicleParams } from './index'

function VehicleView({ id }: VehicleParams) {
  const dispatch = useDispatch()
  const detailsData = useSelector(
    (state: RootState) => state.details,
    shallowEqual,
  )
  const { details, makes, models, status, error } = detailsData
  const isLoading = status === FETCHING_STATUS.LOADING.toString()

  useEffect(() => {
    dispatch(DetailsActions.loadDetails(id))
  }, [id])

  if (error) {
    return <Alert variant="danger">{error}</Alert>
  }

  return isLoading ? (
    <Loading />
  ) : (
    <>
      <pre>{JSON.stringify(details, null, 2)}</pre>
      <hr />
      <h3>Makes</h3>
      <pre>{JSON.stringify(makes, null, 2)}</pre>
      <hr />
      <h3>Models</h3>
      <pre>{JSON.stringify(models, null, 2)}</pre>
    </>
  )
}

export default VehicleView
