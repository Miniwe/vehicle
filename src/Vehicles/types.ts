export type VehiclesListItemParams = {
  Mfr_ID: string
  Country: string
  Mfr_Name: string
}

export type VehicleParams = { id: string }
