import React from 'react'
import { Alert } from 'react-bootstrap'

function Loading({ message = 'Loading ...' }) {
  return <Alert variant="info">{message}</Alert>
}

export default Loading
