import React from 'react'
import { NavLink } from 'react-router-dom'
import { Container, Navbar, NavItem, Nav } from 'react-bootstrap'

function Header() {
  return (
    <Navbar bg="dark" variant="dark" className="top-navbar">
      <Container>
        <Navbar.Brand href="#home">Vehicle's</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Nav className="mr-auto">
          <NavItem>
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
          </NavItem>
          <NavItem>
            <Nav.Link as={NavLink} to="/table">
              Table
            </Nav.Link>
          </NavItem>
          <NavItem>
            <Nav.Link as={NavLink} to="/details">
              Details
            </Nav.Link>
          </NavItem>
        </Nav>
      </Container>
    </Navbar>
  )
}

export default Header
