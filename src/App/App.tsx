import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import { Home, Table, Details, NotFound } from '../pages'
import { Header } from '../components'

function App() {
  return (
    <Router>
      <Header />
      <Container as="main">
        <Switch>
          <Route path="/table">
            <Table />
          </Route>
          <Route path="/details/:id">
            <Details />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </Container>
    </Router>
  )
}

export default App
