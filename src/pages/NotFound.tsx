import React from 'react'
import { Alert } from 'react-bootstrap'

function NotFound() {
  return (
    <Alert variant="danger">
      <h4>404</h4>
      Page Not Found
    </Alert>
  )
}

export default NotFound
