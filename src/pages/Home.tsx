import React from 'react'

function Home() {
  return (
    <section>
      <header>
        <h1>Vehicle's</h1>
      </header>
      <p>
        A web application with a list of vehicle manufacturers which I can click
        on to see them in detail.
      </p>
      <p>
        The whole application is utilising publicly accessible data{' '}
        <a
          href="https://vpic.nhtsa.dot.gov/api/"
          target="_blank"
          rel="noopener noreferrer nofollow"
        >
          https://vpic.nhtsa.dot.gov/api/
        </a>{' '}
        .
      </p>
      <p>&nbsp;</p>
      <p>
        Keep in mind the best practices for building web applications including
        performance.
      </p>
      <hr />
      <a
        href="https://bitbucket.org/Miniwe/vehicle/"
        target="_blank"
        rel="noopener noreferrer nofollow"
      >
        <img src="logo.svg" width="24" height="24" />{' '}
        https://bitbucket.org/Miniwe/vehicle/
      </a>
    </section>
  )
}

export default Home
