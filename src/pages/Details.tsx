import React from 'react'
import { useParams } from 'react-router-dom'
import { VehicleView } from '../Vehicles'

type RouteParams = { id: string }

function Details() {
  const { id } = useParams<RouteParams>()

  return (
    <section>
      <header>
        <h1>Details Page {id}</h1>
        <blockquote>
          The detail page shows me exactly the same data as in the list +
          there's a list of all the model names of all the makes of that one
          manufacturer.
        </blockquote>
      </header>
      <VehicleView id={id} />
    </section>
  )
}

export default Details
