export { default as Home } from './Home'
export { default as Table } from './Table'
export { default as Details } from './Details'
export { default as NotFound } from './NotFound'
