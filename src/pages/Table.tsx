import React from 'react'
import { VehiclesList } from '../Vehicles'

function Table() {
  return (
    <section>
      <header>
        <h1>Table Page</h1>
        <blockquote>
          The list of manufacturers is an infinity loading table with these
          columns: ID, common name and country. Besides that there's an
          additional column with a button which leads to the detail page.
        </blockquote>
      </header>
      <VehiclesList />
    </section>
  )
}

export default Table
